import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route} from "react-router";
import {MyProfile} from "./MyProfile";
import {AboutUs} from "./AboutUs";
import {Home} from "./Home";
import Nav from "./Nav";
import Products from "./Products";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <Nav />
          <Route exact path="/" component={Home} />
          <Route path="/products" component={Products} />
          <Route path="/myProfile" component={MyProfile} />
          <Route path="/aboutUs" component={AboutUs} />
        </Router>
      </div>
    );
  }
}

export default App;
