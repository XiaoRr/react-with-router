import React from 'react';
import {Link} from "react-router-dom";

export function AboutUs() {
  return(
    <div>
      Company: ThoughtWorks Local <br/>
      Location : Xi'an<br/>
      <br/>
      For more information, please<br/>
      view our <Link to={"/"}>website</Link>.<br/>

    </div>
  )
}