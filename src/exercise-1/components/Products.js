import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
const data = require('../Data/data.json')

class Products extends Component {
  constructor(props) {
    super(props);
   // this.handleChange = this.handleChange.bind(this);
    this.state = {
      data : data
    }
  }

  renderProductList(){
    let itemList = [];
    console.log(this.state.data);
    for(let i in this.state.data) {
      itemList.push(<div>
        <Link key = {this.state.data[i].id}
              exact = "true"
              to = {"/products/"+this.state.data[i].id}>
        {this.state.data[i].name}
        </Link></div>)
    }
    return itemList;
  }
  render() {

    return(
      <div>
        {this.renderProductList()}
      </div>
    )
  }
}

export default Products;