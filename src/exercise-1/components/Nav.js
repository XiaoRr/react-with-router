import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Nav extends Component {
  // constructor(props) {
  //   super(props);
  //  // this.handleChange = this.handleChange.bind(this);
  // }

  render() {
    return(
      <nav>
        <NavLink activeClassName = "activeNav" exact to = "/"> Home</NavLink>
        <NavLink activeClassName = "activeNav" to = "/products"> Products</NavLink>
        <NavLink activeClassName = "activeNav" to = "/myProfile"> Profile</NavLink>
        <NavLink activeClassName = "activeNav" to = "/aboutUs"> About Us</NavLink>
      </nav>
    )
  }
}

export default Nav;